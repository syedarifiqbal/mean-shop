const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true},
    description: { type: String, required: true},
    price: Number,
    status: Boolean,
    author: { type: String, required: true },
    publisher: { type: String, required: true },
    category: { type: String, required: true },
    isbn: { type: String, required: true },
    language: { type: String, required: true },
    edition: { type: String, required: true },
    storeCountry: { type: String, required: true }
});

module.exports = mongoose.model('Product', productSchema);