const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: { type: Date, default: new Date() },
    name: String,
    email: String,
    address: String,
    city: String,
    country: String,
    zipcode: String,
    description: String,
    amount: { type: Number, required: true },
    productsQty: [ { type: Number, default: 1 } ],
    products: [ { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Product' } ],
    userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' }
})

module.exports = mongoose.model('Order', productSchema);