const mongoose = require('mongoose');

const storeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true},
    address: { type: String, required: true},
    contact: { type: String, required: true},
    country: { type: String, required: true },
    lat: { type: Number, required: true },
    lng: { type: Number, required: true }
})

module.exports = mongoose.model('Store', storeSchema);