const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const UserController = require('../controllers/users');

// and these are related to users and it's profile. like signin or sign up.

router.get('/', auth, UserController.index);

router.post('/signup', UserController.signup);

router.post('/signin', UserController.signin);

router.delete('/:userId', auth, UserController.destroy);

router.get('/profile', auth, UserController.profile);

module.exports = router;