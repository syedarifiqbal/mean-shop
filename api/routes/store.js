const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const StoreController = require('../controllers/stores');

router.get('/', StoreController.index);

router.get('/countries', StoreController.countries);

router.get('/:storeId', auth, StoreController.show);

router.get('/country/:countryName', StoreController.getStoresByCountryName);

router.put('/:storeId', auth, StoreController.update);

router.post('/', auth, StoreController.store);

router.delete('/:storeId', auth, StoreController.destroy);

module.exports = router;