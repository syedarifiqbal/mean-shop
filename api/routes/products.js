const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/products');

//  localhost:3000/products/     // same as order
router.get('/', ProductController.index);

//  router.get('/:q', ProductController.search);

//you can see all the routes here (this is your api or back-end)

router.post('/', ProductController.store);

router.get('/:productId', ProductController.show);

router.patch('/:productId', ProductController.update);

router.put('/:productId', ProductController.update);

router.delete('/:productId', ProductController.destroy);

module.exports = router;