const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const OrderController = require('../controllers/orders');

// those are end-point related to orders

router.get('/', auth, OrderController.index);

router.get('/:orderId', auth, OrderController.show);

router.post('/', auth, OrderController.store);

router.delete('/:orderId', auth, OrderController.destroy);

module.exports = router;