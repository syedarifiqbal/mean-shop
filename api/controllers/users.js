const User = require('../models/user');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

/**
 * Retrive All User
 * @method GET
 * @return {}
 */
exports.index = (req, res, next) => {

    let page = req.query.page || 1;
    page = parseInt(page) || 1;
    const perPage = 10;
    const offset = (page - 1) * perPage;

    User.estimatedDocumentCount().then(totalRecords => { 

        User.find().skip(offset).limit(perPage).exec()
        .then(docs => {
            res.send({
                data: docs,
                meta: {
                    perPage: perPage,
                    currentPage: page,
                    total: totalRecords
                }
            }).status(200);
        }).catch(error => {
            console.log(error);
            res.send({ error: error, code: 500 }).status(500);
        });

    }).catch(error => {
        console.log(error);
        res.send({ error: error, code: 500 }).status(500);
    });

}

/**
 * Sign in with email and password
 * @method POST
 * @return {}
 */
exports.signin = (req, res, next) => {

    User.findOne({ email: req.body.email, status: true })
    .exec()
    .then(user => {
        if(!user)
            return res.status(422).json({ message: "Authentication failed!" });

        bcrypt.compare(req.body.password, user.password, (error, success) => {
            if(success)
            {
                const token = jwt.sign({ _id: user._id, email: user.email, name: user.name }, process.env.JWT_KEY, { expiresIn: 60 * 60 * 60 * 60 })
                return res.status(200).json({
                    message: "authenticated successfully",
                    user: {
                        name: user.name,
                        email: user.email
                    },
                    status: 200,
                    'token': token
                });
            }
            return res.status(422).json({ message: "Authentication failed!" });
        });
    }).catch(error => res.status(422).json({ message: "Authentication failed!" }));

};

/**
 * Save/Insert Product to database
 * @method POST
 * @param data
 * @return {}
 */
exports.signup = (req, res, next) => {

    User.find({ email: req.body.email })
    .exec()
    .then(users => {
        if(users.length > 0)
        {
            return res.status(422).json({ message: "Email already exists in system." });
        }

        bcrypt.hash(req.body.password, 10)
        .then(hash => {
            // password is now hashed we can go ahead and insert new user.
            const user = new User({
                _id: new mongoose.Types.ObjectId,
                name: req.body.name,
                email: req.body.email,
                password: hash,
                status: req.body.status ? !!req.body.status: true
            });
        
            user.save()
            .then(result => {
                res.send({
                    message: "Signed up Successfully!",
                    status: 201
                }).status(201);
            })
            .catch(error => {
                res.send(error).status(500);
            } );
        })
        .catch(error => {
            res.send(error).status(500);
        })
    })

};

/**
 * Get Single Product from database
 * @method get
 * @return {}
 */
exports.profile = (req, res, next) => {
    User.findById( req.user._id ).exec()
    .then(doc => {
        res.send(doc).status(200);
    }).catch(error => {
        res.send({ error: error, code: 500 }).status(500);
    });
}

/**
 * Save/Insert Product to database
 * @method DELETE
 * @param productId
 * @return {}
 */
exports.destroy = (req, res, next) => {

    User.deleteOne({ _id: req.params.userId }).exec()
        .then(success => {
            if( !! success.n )
                return res.json({ message: "User Deleted!"}).status(200);
            return res.json({ message: "User not found!" }).status(404);
        }).catch(error => {
            return res.json({ error: error, code: 500 }).status(500);
        });
}