const Product = require('../models/product');
const mongoose = require('mongoose');

/**
 * Retrive All Products
 * @method GET
 * @return {}
 */
exports.index = (req, res, next) => {
    let page = req.query.page || 1;
    page = parseInt(page) || 1;
    const perPage = 12;
    const offset = (page - 1) * perPage;
    Product.estimatedDocumentCount().then(totalRecords => { 
        Product.find().skip(offset).limit(perPage).exec()
        .then(docs => {
            res.send({
                data: docs,
                meta: {
                    perPage: perPage,
                    currentPage: page,
                    total: totalRecords
                }
            }).status(200);
        }).catch(error => {
            console.log(error);
            res.send({ error: error, code: 500 }).status(500);
        });

    }).catch(error => {
        console.log(error);
        res.send({ error: error, code: 500 }).status(500);
    });
}
/**
 * Retrive All Products
 * @method GET
 * @return {}
 */
exports.search = (req, res, next) => {
    const query = req.query.q || '';
    let page = req.query.page || 1;
    page = parseInt(page) || 1;
    const perPage = 12;
    const offset = (page - 1) * perPage;
    
    let filters = [
        { 
            name: new RegExp(query, 'i'),
        },
        { 
            description: new RegExp(query, 'i') 
        }
    ];

    if(parseInt(query)) { filters.push( { price: parseInt(query) } ); }
    
    filters = { $or: filters };

    Product.countDocuments(filters).then(totalRecords => {
        
        Product.find(filters)
            .skip(offset).limit(perPage)
            .exec()
            .then(docs => {
                res.send({
                    data: docs,
                    meta: {
                        perPage: perPage,
                        currentPage: page,
                        total: totalRecords
                    }
                }).status(200);
            }).catch(error => {
                console.log(error);
                res.send({ error: error, code: 500 }).status(500);
            });

    }).catch(error => {
        console.log(error);
        res.send({ error: error, code: 500 }).status(500);
    });
}

/**
 * Save/Insert Product to database
 * @method POST
 * @param data
 * @return {}
 */
exports.store = (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId,
        name: req.body.name,
        description: req.body.description ? req.body.description: '',
        price: req.body.price,
        status: req.body.status ? !!req.body.status: true,
        author: req.body.author,
        language: req.body.language,
        isbn: req.body.isbn,
        edition: req.body.edition,
        category: req.body.category,
        publisher: req.body.publisher,
        storeCountry: req.body.storeCountry,
    });

    product.save()
    .then(result => {
        res.send({
            message: "Product Created Successfully!",
            status: 201
        }).status(201);
    })
    .catch(error => {
        res.status(500).send({
            error: error,
            status: 500
        });
    });
}

/**
 * Get Single Product from database
 * @method get
 * @param productId
 * @return {}
 */
exports.show = (req, res, next) => {
    Product.findById( req.params.productId ).exec()
        .then(doc => {
            res.send(doc).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Save/Insert Product to database
 * @method PATCH/PUT
 * @param productId
 * @param data
 * @return {}
 */
exports.update = (req, res, next) => {
    Product.update({ _id: req.params.productId }, req.body).exec()
        .then(success => {
            res.send(success).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Save/Insert Product to database
 * @method DELETE
 * @param productId
 * @return {}
 */
exports.destroy = (req, res, next) => {

    Product.remove({ _id: req.params.productId }).exec()
        .then(success => {
            res.send({ message: "Product Deleted!"}).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}