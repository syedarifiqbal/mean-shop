const Order = require('../models/order');
const Product = require('../models/product');
const Store = require('../models/store');
const mongoose = require('mongoose');

/**
 * Retrive All Products
 * @method GET
 * @return {}
 */
exports.index = (req, res, next) => {
    let page = req.query.page || 1;
    page = parseInt(page) || 1;
    const perPage = 100;
    const offset = (page - 1) * perPage;
    Store.estimatedDocumentCount().then(totalRecords => { 
        Store.find({}).skip(offset).limit(perPage).exec()
        .then(stores => {
            res.send({
                data: stores.map( store => {

                    return {
                        storeId: store._id,
                        name: store.name,
                        address: store.address,
                        lat: store.lat,
                        lng: store.lng,
                        address: store.address,
                        country: store.country
                    }
                }),

                meta: {
                    perPage: perPage,
                    currentPage: page,
                    total: totalRecords
                }

            }).status(200);

        }).catch(error => res.send({ error: error, code: 500 }).status(500) );

    }).catch(error => res.send({ error: error, code: 500 }).status(500) );
}

/**
 * Save/Insert Product to database
 * @method POST
 * @param data
 * @return {}
 */
exports.store = (req, res, next) => {

    const store = new Store({
        _id: new mongoose.Types.ObjectId,
        name: req.body.name,
        country: req.body.country,
        address: req.body.address,
        lat: req.body.lat,
        lng: req.body.lng,
        contact: req.body.contact
    });

    store.save()
    .then(result => {
        res.send({
            result: result,
            message: "Store Created Successfully!",
            status: 201
        }).status(201);
    })
    .catch(error => {
        res.send(error).status(201);
    });
}

/**
 * Get Single Product from database
 * @method get
 * @param orderId
 * @return {}
 */
exports.show = (req, res, next) => {
    Order.findById( req.params.orderId ).exec()
        .then(order => {
            res.send(order).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Get Single Product from database
 * @method get
 * @param countryName
 * @return {}
 */
exports.getStoresByCountryName = (req, res, next) => {
    Store.find( { 'country' : req.params.countryName } ).exec()
        .then(stores => {
            res.send(stores).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Save/Insert Product to database
 * @method PATCH/PUT
 * @param productId
 * @param data
 * @return {}
 */
exports.update = (req, res, next) => {
    Store.update({ _id: req.params.storeId }, req.body).exec()
        .then(success => {
            res.send(success).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Save/Insert Product to database
 * @method DELETE
 * @param orderId
 * @return {}
 */
exports.destroy = (req, res, next) => {

    Order.remove({ _id: req.params.orderId }).exec()
        .then(success => {
            res.send({ message: "Order Deleted!"}).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Get Available Countries from store
 * @method get
 * @return {}
 */
exports.countries = (req, res, next) => {
    Store.find({}).distinct('country', (error, response) => {
        if(error)
        {
            return res.send({ error: error, code: 500 }).status(500);
        }
        return res.send(response).status(200);
    });
}