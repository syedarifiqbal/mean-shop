const Order = require('../models/order');
const Product = require('../models/product');
const mongoose = require('mongoose');

/**
 * Retrive All Products
 * @method GET
 * @return {}
 */
exports.index = (req, res, next) => {
    let page = req.query.page || 1;
    page = parseInt(page) || 1;
    const perPage = 10;
    const offset = (page - 1) * perPage;
    Order.estimatedDocumentCount().then(totalRecords => { 
        Order.find({ userId: req.user._id }).skip(offset).limit(perPage).exec()
        .then(orders => {
            res.send({
                data: orders.map( o => {
                    return {
                        orderId: o._id,
                        products: o.products,
                        orderDate: o.date
                    }
                }),
                meta: {
                    perPage: perPage,
                    currentPage: page,
                    total: totalRecords
                }
            }).status(200);
        }).catch(error => {
            console.log(error);
            res.send({ error: error, code: 500 }).status(500);
        });

    }).catch(error => {
        console.log(error);
        res.send({ error: error, code: 500 }).status(500);
    });
}

/**
 * Save/Insert Product to database
 * @method POST
 * @param data
 * @return {}
 */
exports.store = (req, res, next) => {

    const products = req.body.products;

    Product.find({ _id: { $in: products.map(product => mongoose.Types.ObjectId(product._id)) } })
    .exec()
    .then(productsFound => {

        if(productsFound.length !== products.length){
            res.json({ message: "products not found or multiple products added at the same time" }).status(422);
            return;
        }

        const order = new Order({
            _id: new mongoose.Types.ObjectId,
            date: new Date(),
            description: req.body.description,
            amount: req.body.amount,
            products: req.body.products,
            userId: req.user._id,
            storeId: req.body.store_id
        });
    
        order.save()
        .then(result => {
            res.send({
                result: result,
                message: "Order Created Successfully!",
                status: 201
            }).status(201);
        })
        .catch(error => {
            res.send(error).status(201);
        });
    })
    .catch(error => {
        console.log(error);
        res.json(error).status(422);
    })
}

/**
 * Get Single Product from database
 * @method get
 * @param orderId
 * @return {}
 */
exports.show = (req, res, next) => {
    Order.findById( req.params.orderId ).exec()
        .then(order => {
            res.send(order).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Save/Insert Product to database
 * @method PATCH/PUT
 * @param productId
 * @param data
 * @return {}
 */
exports.update = (req, res, next) => {
    Product.update({ _id: req.params.productId }, req.body).exec()
        .then(success => {
            res.send(success).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}

/**
 * Save/Insert Product to database
 * @method DELETE
 * @param orderId
 * @return {}
 */
exports.destroy = (req, res, next) => {

    Order.remove({ _id: req.params.orderId }).exec()
        .then(success => {
            res.send({ message: "Order Deleted!"}).status(200);
        }).catch(error => {
            res.send({ error: error, code: 500 }).status(500);
        });
}