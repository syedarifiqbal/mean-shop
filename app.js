const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors')

app.use(cors({
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }));

// console.log(process.env.MONGO_DB_PASSWORD);
mongoose.connect(`mongodb://mean-rest-api:mean-rest-api@mean-rest-api-shard-00-00-x2js3.mongodb.net:27017,mean-rest-api-shard-00-01-x2js3.mongodb.net:27017,mean-rest-api-shard-00-02-x2js3.mongodb.net:27017/test?ssl=true&replicaSet=mean-rest-api-shard-0&authSource=admin&retryWrites=true`, 
    { 
        useNewUrlParser: true
        // userMongoClient: true
    }
);

const sotreRoutes = require('./api/routes/store');
const productRoutes = require('./api/routes/products');
const userRoutes = require('./api/routes/user');
const orderRoutes = require('./api/routes/order');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/api/products', productRoutes);
app.use('/api/user', userRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/stores', sotreRoutes);

app.use((request, response, next) => {
    const error = new Error("Not found!");
    error.status = 404;
    next(error);
});

app.use((error, request, response, next) => {
    response.status( error.status || 500 );

    response.json({
        message: error.message,
        errorCode: error.status
    })
})

module.exports = app;